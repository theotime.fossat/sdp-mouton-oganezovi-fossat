import pickle
from abc import abstractmethod

import numpy as np
import gurobipy as gp

from sklearn.cluster import KMeans
from sklearn.linear_model import LinearRegression


class BaseModel(object):
    """
    Base class for models, to be used as coding pattern skeleton.
    Can be used for a model on a single cluster or on multiple clusters"""

    def __init__(self):
        """Initialization of your model and its hyper-parameters"""
        pass

    @abstractmethod
    def fit(self, X, Y):
        """Fit function to find the parameters according to (X, Y) data.
        (X, Y) formatting must be so that X[i] is preferred to Y[i] for all i.

        Parameters
        ----------
        X: np.ndarray
            (n_samples, n_features) features of elements preferred to Y elements
        Y: np.ndarray
            (n_samples, n_features) features of unchosen elements
        """
        # Customize what happens in the fit function
        return

    @abstractmethod
    def predict_utility(self, X):
        """Method to call the decision function of your model

        Parameters:
        -----------
        X: np.ndarray
            (n_samples, n_features) list of features of elements

        Returns
        -------
        np.ndarray:
            (n_samples, n_clusters) array of decision function value for each cluster.
        """
        # Customize what happens in the predict utility function
        return

    def predict_preference(self, X, Y):
        """Method to predict which pair is preferred between X[i] and Y[i] for all i.
        Returns a preference for each cluster.

        Parameters
        -----------
        X: np.ndarray
            (n_samples, n_features) list of features of elements to compare with Y elements of same index
        Y: np.ndarray
            (n_samples, n_features) list of features of elements to compare with X elements of same index

        Returns
        -------
        np.ndarray:
            (n_samples, n_clusters) array of preferences for each cluster. 1 if X is preferred to Y, 0 otherwise
        """
        X_u = self.predict_utility(X)
        Y_u = self.predict_utility(Y)

        return (X_u - Y_u > 0).astype(int)

    def predict_cluster(self, X, Y):
        """Predict which cluster prefers X over Y THE MOST, meaning that if several cluster prefer X over Y, it will
        be assigned to the cluster showing the highest utility difference). The reversal is True if none of the clusters
        prefer X over Y.
        Compared to predict_preference, it indicates a cluster index.

        Parameters
        -----------
        X: np.ndarray
            (n_samples, n_features) list of features of elements to compare with Y elements of same index
        Y: np.ndarray
            (n_samples, n_features) list of features of elements to compare with X elements of same index

        Returns
        -------
        np.ndarray:
            (n_samples, ) index of cluster with highest preference difference between X and Y.
        """
        X_u = self.predict_utility(X)
        Y_u = self.predict_utility(Y)

        return np.argmax(X_u - Y_u, axis=1)

    def save_model(self, path):
        """Save the model in a pickle file. Don't hesitate to change it in the child class if needed

        Parameters
        ----------
        path: str
            path indicating the file in which the model will be saved
        """
        with open(path, "wb") as f:
            pickle.dump(self, f)

    @classmethod
    def load_model(clf, path):
        """Load a model saved in a pickle file. Don't hesitate to change it in the child class if needed

        Parameters
        ----------
        path: str
            path indicating the path to the file to load
        """
        with open(path, "rb") as f:
            model = pickle.load(f)
        return model


class RandomExampleModel(BaseModel):
    """Example of a model on two clusters, drawing random coefficients.
    You can use it to understand how to write your own model and the data format that we are waiting for.
    This model does not work well but you should have the same data formatting with TwoClustersMIP.
    """

    def __init__(self):
        self.seed = 444
        self.weights = self.instantiate()

    def instantiate(self):
        """No particular instantiation"""
        return []

    def fit(self, X, Y):
        """fit function, sets random weights for each cluster. Totally independant from X & Y.

        Parameters
        ----------
        X: np.ndarray
            (n_samples, n_features) features of elements preferred to Y elements
        Y: np.ndarray
            (n_samples, n_features) features of unchosen elements
        """
        np.random.seed(self.seed)
        num_features = X.shape[1]
        weights_1 = np.random.rand(num_features) # Weights cluster 1
        weights_2 = np.random.rand(num_features) # Weights cluster 2

        weights_1 = weights_1 / np.sum(weights_1)
        weights_2 = weights_2 / np.sum(weights_2)
        self.weights = [weights_1, weights_2]
        return self

    def predict_utility(self, X):
        """Simple utility function from random weights.

        Parameters:
        -----------
        X: np.ndarray
            (n_samples, n_features) list of features of elements

        Returns
        -------
        np.ndarray:
            (n_samples, n_clusters) array of decision function value for each cluster.
        """
        u_1 = np.dot(X, self.weights[0]) # Utility for cluster 1 = X^T.w_1
        u_2 = np.dot(X, self.weights[1]) # Utility for cluster 2 = X^T.w_2
        return np.stack([u_1, u_2], axis=1) # Stacking utilities over cluster on axis 1


class TwoClustersMIP(BaseModel):
    """Skeleton of MIP you have to write as the first exercise.
    You have to encapsulate your code within this class that will be called for evaluation.
    """

    def __init__(self, n_pieces, n_clusters):
        """Initialization of the MIP Variables

        Parameters
        ----------
        n_pieces: int
            Number of pieces for the utility function of each feature.
        n_clusters: int
            Number of clusters to implement in the MIP.
        """
        self.seed = 123
        self.model = self.instantiate()

    def instantiate(self):
        """Instantiation of the MIP Variables - To be completed."""
        # To be completed
        return

    def fit(self, X, Y, n = 4, K = 2, L = 5, P = 400, M=3, eps= 0.001):
        """Estimation of the parameters - To be completed.

        Parameters
        ----------
        X: np.ndarray
            (n_samples, n_features) features of elements preferred to Y elements
        Y: np.ndarray
            (n_samples, n_features) features of unchosen elements
        """

        # VARIABLES

        self.model = gp.Model()
        
        # variables
        self.u = {
            (i,k,l): self.model.addVar(lb=0,vtype='C', name=f'u_{i}-{k}-{l}') for i in range(n) for k in range(K) for l in range(L+1)
        }

        self.z = {
            (k, j): self.model.addVar(vtype='B', name=f'z_{k}-{j}') for k in range(K) for j in range(P)
        }

        self.sigma_x_plus = {
            (j) : self.model.addVar(lb=0, vtype='C', name=f'\u03C3^+(x^{j})') for j in range(P)
        }
        self.sigma_y_plus = {
            (j) : self.model.addVar(lb=0, vtype='C', name=f'\u03C3^+(y^{j})') for j in range(P)
        }

        self.sigma_x_minus = {
            (j) : self.model.addVar(lb=0, vtype='C', name=f'\u03C3^-(x^{j})') for j in range(P)
        }
        self.sigma_y_minus = {
            (j) : self.model.addVar(lb=0, vtype='C', name=f'\u03C3^-(y^{j})') for j in range(P)
        }
        

        # CONSTRAINTS
        for j in range(P):
            sum_z = gp.quicksum([self.z[(k, j)] for k in range(K)])
            self.model.addConstr(sum_z >= 1)

        for i in range(n):
            for k in range(K):
                self.model.addConstr(self.u[(i, k, 0)] == 0)
        
        for k in range(K):
            sum_u = gp.quicksum([self.u[(i, k, L)] for i in range(n)])
            self.model.addConstr(sum_u == 1)
        
        for k in range(K):
            for i in range(n):
                for l in range(L):
                    self.model.addConstr(self.u[(i, k, l+1)] - self.u[(i, k, l)]>=eps)

        MIN = 0 # dans l'absolu max X et Y c'est la mm valeur vu que c'est juste des articles)
        MAX = 1

        def x_l_i(l):
            return MIN + l * (MAX - MIN) / L
    
        def u_k_i(k,i,x):
            l = x//((MAX-MIN)/L)

            return self.u[(i,k,l)] + (x-x_l_i(l))*(self.u[(i,k,l+1)]-self.u[(i,k,l)])/(x_l_i(l+1)-x_l_i(l))

        for j in range(P):
            for k in range(K):
                sum_u = (gp.quicksum([u_k_i(k,i,X[j,i]) for i in range(n)]) - self.sigma_x_plus[j] + self.sigma_x_minus[j]) - (gp.quicksum([u_k_i(k,i,Y[j,i]) for i in range(n)]) - self.sigma_y_plus[j] + self.sigma_y_minus[j])
                self.model.addConstr(sum_u >= -M*(1-self.z[(k,j)]))
                # self.model.addConstr(sum_u <= M*self.z[(k,j)]-self.vars['eps']) # A ENLEVER CAR N=3 APRES
        
        # OBJECTIVE FUNCTION
        objective = gp.quicksum([self.sigma_x_plus[j] + self.sigma_x_minus[j] + 
                                self.sigma_y_plus[j] + self.sigma_y_minus[j] 
                                for j in range(P)])

        self.model.setObjective(objective, gp.GRB.MINIMIZE)

        self.model.optimize()
        
        return self
    

    def predict_utility(self, X):
        """Return Decision Function of the MIP for X.

        Parameters:
        -----------
        X: np.ndarray
            (n_samples, n_features) list of features of elements
        
        Returns:
        -------
        np.ndarray:
            (n_samples, n_clusters) array of decision function value for each cluster.
        """
        
        L = 5
        MIN = 0 
        MAX = 1

        def x_l_i(l):
            return MIN + l * (MAX - MIN) / L

        def u_k_i(k, i, x):
            l = x//((MAX-MIN)/L)
            return self.u[(i, k, l)].X + (x - x_l_i(l)) * (self.u[(i, k, l + 1)].X - self.u[(i, k, l)].X) / (x_l_i(l + 1) - x_l_i(l))
        
        def utility(x, cluster):
            # Somme des expressions linéaires pour chaque feature dans x
            return sum(u_k_i(cluster, i, x_i) for i, x_i in enumerate(x))

        # Calcul de l'utilité pour chaque échantillon dans X pour chaque cluster
        u_1 = np.array([utility(x, 0) for x in X])
        u_2 = np.array([utility(x, 1) for x in X])

        # Nous retournons un tableau de ces expressions linéaires
        return np.stack([u_1, u_2], axis=1)


class HeuristicModel(BaseModel):
    """Skeleton of MIP you have to write as the first exercise.
    You have to encapsulate your code within this class that will be called for evaluation.
    """

    def __init__(self, n_clusters):
        """Initialization of the Heuristic Model.
        """
        self.seed = 123
        self.K = n_clusters

        self.model = self.instantiate()

    def instantiate(self):
        """Instantiation of the MIP Variables"""
        pass

    def fit(self, X, Y, n = 10, K = 3, L = 5, P = 400, M=3, eps= 0.001):
        """Estimation of the parameters - To be completed.

        Parameters
        ----------
        X: np.ndarray
            (n_samples, n_features) features of elements preferred to Y elements
        Y: np.ndarray
            (n_samples, n_features) features of unchosen elements
        """

        # Construction des vecteurs de différence D
        preferences = X - Y  # Assurez-vous que X et Y sont des np.array pour permettre cette opération

        # Clustering avec k-means
        kmeans = KMeans(n_clusters=3, random_state=0)
        kmeans.fit(preferences)
        clusters = kmeans.predict(preferences)

        def fit_cluster(X, Y, n = 10, L = 5,  M=3, eps= 0.001):
            
            model = gp.Model()
            
            #nombre de produit
            P = X.shape[0]

            # variables
            u = {
                (i,l): model.addVar(lb=0,vtype='C', name=f'u_{i}-{l}') for i in range(n) for l in range(L+1)
            }

            sigma_x_plus = {
                (j) : model.addVar(lb=0, vtype='C', name=f'\u03C3^+(x^{j})') for j in range(P)
            }
            sigma_y_plus = {
                (j) : model.addVar(lb=0, vtype='C', name=f'\u03C3^+(y^{j})') for j in range(P)
            }

            sigma_x_minus = {
                (j) : model.addVar(lb=0, vtype='C', name=f'\u03C3^-(x^{j})') for j in range(P)
            }
            sigma_y_minus = {
                (j) : model.addVar(lb=0, vtype='C', name=f'\u03C3^-(y^{j})') for j in range(P)
            }

            # CONSTRAINTS
            for i in range(n):
                model.addConstr(u[(i, 0)] == 0)
            

            sum_u = gp.quicksum([u[(i, L)] for i in range(n)])
            model.addConstr(sum_u == 1)
            
            for i in range(n):
                for l in range(L):
                    model.addConstr(u[(i, l+1)] - u[(i, l)]>=eps)

            MIN = 0 # dans l'absolu max X et Y c'est la mm valeur vu que c'est juste des articles)
            MAX = 1

            def x_l_i(l):
                return MIN + l * (MAX - MIN) / L
        
            def u_i(i,x):
                l = x//((MAX-MIN)/L)

                return u[(i,l)] + (x-x_l_i(l))*(u[(i,l+1)]-u[(i,l)])/(x_l_i(l+1)-x_l_i(l))

            for j in range(P):
                sum_u = (gp.quicksum([u_i(i,X[j,i]) for i in range(n)]) - sigma_x_plus[j] + sigma_x_minus[j]) \
                        - (gp.quicksum([u_i(i,Y[j,i]) for i in range(n)]) - sigma_y_plus[j] + sigma_y_minus[j])
                model.addConstr(sum_u >=0)
            
            # OBJECTIVE FUNCTION
            objective = gp.quicksum([sigma_x_plus[j] + sigma_x_minus[j] + 
                                    sigma_y_plus[j] + sigma_y_minus[j] 
                                    for j in range(P)])

            model.setObjective(objective, gp.GRB.MINIMIZE)

            model.optimize()
        
            poids_u = {
                (i,l) : u[(i,l)].X for i in range(n) for l in range(L+1)
            }

            return poids_u        

        poids_u = [fit_cluster(X[clusters==k], Y[clusters==k]) for k in range(3)]
        
        self.u = {(i,k,l):poids_u[k][(i,l)] for i in range(n) for k in range(K) for l in range(L+1)}

        return
    

    def predict_utility(self, X):
        """Return Decision Function of the MIP for X. - To be completed.

        Parameters:
        -----------
        X: np.ndarray
            (n_samples, n_features) list of features of elements
        
        Returns
        -------
        np.ndarray:
            (n_samples, n_clusters) array of decision function value for each cluster.
        """

        L = 5
        MIN = 0 
        MAX = 1

        def x_l_i(l):
            return MIN + l * (MAX - MIN) / L

        def u_k_i(k, i, x):
            l = x//((MAX-MIN)/L)
            return self.u[(i, k, l)] + (x - x_l_i(l)) * (self.u[(i, k, l + 1)] - self.u[(i, k, l)]) / (x_l_i(l + 1) - x_l_i(l))
        
        def utility(x, cluster):
            # Somme des expressions linéaires pour chaque feature dans x
            return sum(u_k_i(cluster, i, x_i) for i, x_i in enumerate(x))

        # Calcul de l'utilité pour chaque échantillon dans X pour chaque cluster
        u_1 = np.array([utility(x, 0) for x in X])
        u_2 = np.array([utility(x, 1) for x in X])

        # Nous retournons un tableau de ces expressions linéaires
        return np.stack([u_1, u_2], axis=1)
