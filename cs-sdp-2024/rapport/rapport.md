Théotime Fossat
Louis Mouton
Giorgi Oganezovi


# Clustering and modeling customer preferences in a supermarket

## Introduction 

L'objectif de ce rapport est de présenter notre approche pour comprendre et modéliser les préférences des clients dans un contexte de ventes en grande surface, en utilisant des techniques avancées de Programmation Mixte en Nombres Entiers (MIP) et de modèles heuristiques. On vise à _clusteriser_ les clients en fonction de leurs comportements d'achat et à apprendre les fonctions d'utilités spécifiques à chaque groupe. Ce faisant, nous explorons comment ces modèles peuvent apporter une compréhension plus profonde des décisions des consommateurs.

## I. Formulation MIP pour l'Apprentissage des Préférences

### Objectif
Apprendre les fonctions de décision \(u_k\) pour chaque cluster \(k\), en minimisant les écarts entre les produits choisis et non choisis tout en respectant la structure des préférences exprimées par les données.

### Notations et Variables
- \(K\): Nombre de clusters
- \(n\): Nombre de critères
- \(L\): Nombre de segments linéaires
- \(u_{i}^{k,l}\): Valeurs marginales aux points de rupture pour le critère \(i\), cluster \(k\), et segment \(l\)
- \(P\): Nombre de pairs d'apprentissage
- \(z_{k,j}\): Variable binaire pour l'assignation des paires aux clusters
- \(\sigma^+(x^{j})\), \(\sigma^-(x^{j})\), \(\sigma^+(y^{j})\), \(\sigma^-(y^{j})\): Écarts d'ajustement

### Contraintes
1. Chaque paire \(j\) est représentée par au moins un cluster \(k\).

\[\forall j \in \llbracket 1, P\rrbracket, \sum_{k=1}^{K} z_k^j \geq 1\]

2. La valeur marginale au premier point de rupture est zéro pour chaque critère et cluster.

\[\forall k \in \llbracket 1, K\rrbracket,\forall i \in \llbracket 1, N\rrbracket,  u_{i}^{k,0} = 0\]


3. La somme des valeurs marginales au dernier point de rupture est égale à un pour chaque critère dans chaque cluster.


\[\forall k \in \llbracket 1, K \rrbracket,\forall i \in \llbracket 1, N\rrbracket, \sum_{i=1}^{n} u_{i}^{k,L} = 1\]



4. Les valeurs marginales sont croissantes le long des segments.

    \[\forall k \in \llbracket 1, K \rrbracket, \forall i \in \llbracket 1, n \rrbracket, \forall l \in \llbracket 0, L-1 \rrbracket, u_{i}^{k,l+1} - u_{i}^{k,l} \geq \epsilon\]

    Avec ε arbitrairement petit.
    

5. Chaque produit du cluster \(X\) a été préféré au produit du cluster \(Y\) tout en tenant compte des erreurs de mesure ou de l'incertitude dans l'estimation des fonctions d'utilités à travers les variables \(\sigma^+\) et \(\sigma^-\)

    \[\forall j \in \llbracket 1, P \rrbracket, \forall k \in \llbracket 1, K \rrbracket, \sum_{i=1}^{n} \left( u_i^k(x^{j}_i) \right) - \sigma^+(x^{j}) + \sigma^-(x^{j}) - \sum_{i=1}^{n} \left( u_i^k(y^{j}_i) \right) - \sigma^+(y^{j}) + \sigma^-(y^{j}) \geq -M \cdot (1-z_k^j)\]

    Avec M un réel arbitrairement grand.

### Fonction Objectif
Minimiser la somme des écarts d'ajustement pour toutes les paires, reflétant la précision de l'apprentissage des préférences.

\[\min \sum_{j=1}^{P} \left( \sigma^+(x^{j}) + \sigma^-(x^{j}) + \sigma^+(y^{j}) + \sigma^-(y^{j}) \right)\]

## II. Implémentation de la Fonction fit pour TwoClusterMIP

### Aperçu de l'Implémentation
La méthode `fit` de la classe TwoClusterMIP est conçue pour estimer les paramètres des fonctions de décision pour chaque cluster en utilisant une formulation de programmation linéaire mixte en entiers (MIP). Cette méthode prend en entrée les caractéristiques des produits `X` préférés aux produits`Y`, ainsi que les paramètres de configuration du modèle tels que le nombre de critères `n`, le nombre de clusters `K`, le nombre de segments linéaires `L`, et le nombre de paires d'apprentissage `P`.

### Définition des variables

Les variables ont été dans plusieurs dictionnaires afin de pouvoir facilement les appeler par la suite.

- Variables de décision `u`: Représentent les valeurs marginales aux points de rupture pour chaque critère, cluster, et segment linéaire.
- Variables binaires `z`: Indiquent l'assignation des paires de produits aux clusters.
- Variables d'écart `sigma_x_plus`, `sigma_x_minus`, `sigma_y_plus`, `sigma_y_minus`: Modélisent les écarts positifs et négatifs dans l'estimation des fonctions d'utilité.

### Définition des contraintes

1. **Représentation des Paires par au Moins un Cluster:** Assure que chaque paire de produits est attribuée à au moins un cluster.
2. **Initialisation des Valeurs Marginales:** Fixe la valeur marginale au premier point de rupture à zéro pour chaque critère et cluster.
3. **Normalisation des Valeurs Marginales:** Garantit que la somme des valeurs marginales au dernier point de rupture est égale à un pour chaque critère dans chaque cluster.
4. **Croissance des Valeurs Marginales:** S'assure que les valeurs marginales sont croissantes le long des segments.

### Définition fonction objectif

La fonction objectif vise à minimiser la somme des écarts d'ajustement pour toutes les paires, reflétant la précision avec laquelle le modèle apprend les préférences des clients.

### Processus d'Optimisation

La méthode utilise `gurobipy` (Gurobi Python) pour définir et résoudre le problème MIP. Après la définition des variables, contraintes, et de la fonction objectif, la méthode `optimize` de Gurobi est appelée pour trouver la solution optimale des fonctions de décision.

### Illustration et Exemples

#### Appel de la fonction

`In`
```Python
model_pour_deux_cluster = TwoClustersMIP(Nombre_de_segment = 5, Nombre_cluster = 2)
model_pour_deux_cluster.fit(cluste_pref = X, cluster_rejeté = Y)
```
`Out`
```Python
Best objective 0.000000000000e+00, best bound 0.000000000000e+00, gap 0.0000%
```
Gurobi parvient à trouver une solution exacte, c'est-à-dire, à mettre toutes les erreurs à zéro. Cela vient du fait que le jeu d'entrainement a été généré sans bruit.

### Fonctions d'utilités obtenues

<img src="images\fonctions utilité.png" width="800">
<img src="images\output.png" width="600">

## Défis et Solutions

Nous avons mis beaucoup de temps à formaliser correctement la modélisation du problème sur Gurobi, mais une fois réalisé, on a constaté une résolution rapide et des résultats surprenants. Cependant, nous avons vite rencotntré un problème de convergance pour le dataset_10. Nous résoudrons ce problème en séparant la tâche en deux étapes : la clusterisation via un modèle heuristique, puis l'identification des fonctions d'utilité. 
Pour le paramètre M, nous avons choisi M=3, ce qui était adéquat puisque les fonctions d'utilité se situent entre 0 et 1. Quant à ε, on l'a fixé à 0.001, pour maintenir un équilibre entre précision des résultats et efficacité de calcul.


## III. Implémontation de la Fonction fit pour HeuristicModel

### Aperçu de l'Implémentation
La méthode `fit` de la classe HeuristicModel utilise le fonctionnement de K-Mean pour classer les consomateur. Elle reprend les même fonctionnement entre `X`et `Y` que la fonction `fit` de TwoClusterMIP.

### Implementation de K-Mean
Nous avont créer un object `self.kmean` apartenant à la classe HeuristicModel initalisé avec `self.n_clusters`. 

Après plusieurs test nous nous sommes rendu compte qu'aprendre le `self.kmean` ameliorait les résulats. 


### Processus d'Optimisation

La méthode utilise `keras` pour définir et résoudre le problème avec plusieurs clusters. Après la définition du model, la méthode `fit` de keras est appelée pour trouver la solution optimale des fonctions de décision.

### Illustration et Exemples

#### Appel de la fonction

`In`
```Python
model = HeuristicModel(3) # Instantiation of the model with hyperparameters, if needed
model.fit(X, Y) # Training of the model, using preference data
```

Nous ne pouvons pas ici savoir si keras à trouver une solution exacte. 

### Fonctions d'utilités obtenues

<img src="images\3K_resutls.png" width="800">
<img src="images\3K_output.png" width="600">

